package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");
        precio = (TextView)findViewById(R.id.precio);
        descripcion = (TextView)findViewById(R.id.descripcion);
        nombre = (TextView)findViewById(R.id.nombre);
        thumbnail = (ImageView)findViewById(R.id.thumbnail);

       // precio.setMovementMethod(new ScrollingMovementMethod());
        //descripcion.setMovementMethod(new ScrollingMovementMethod());
        //nombre.setMovementMethod(new ScrollingMovementMethod());



        // INICIO - CODE6
        //recibe todos los parametros

        final DataQuery query = DataQuery.get("item");
       String parametro = getIntent().getExtras().getString("dato");
        query.getInBackground(parametro, new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {
                if (e==null){
                    String price = (String) object.get("price");
                    String description = (String ) object.get("description");
                    String name = (String) object.get("name");
                    Bitmap ImageBitmap = (Bitmap) object.get("image");

                    precio.setText(price + "$");
                    descripcion.setText(description);
                    nombre.setText(name);
                    thumbnail.setImageBitmap(ImageBitmap);
                }
            else {


                }
            }
        });


        // FIN - CODE6

    }
    //se accede a los id
    TextView precio, descripcion, nombre;
    ImageView thumbnail;
    DataObject object_id;


}
